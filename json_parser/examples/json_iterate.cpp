#include <iostream>
#include "../json_parser.h"

double sum_and_print(JsonValue o);

char json[256] = "{\"order\": {"
            "  \"id\": \"1c34-2dvo-2kd9-1wda\","
            "  \"description\": \"delivery\","
            "  \"contains:\": {"
            "    \"item\": ["
            "      {\"id\":1, \"value\": 34500000, \"units\": 1000},"
            "      {\"id\":2, \"value\": 66854000, \"units\": 5000},"
            "      {\"id\":3, \"value\": 55456000, \"units\": 800}"
            "    ]"
            "  }"
            "}}";

int main() {
  char *source = json;
  char *end_ptr;
  JsonValue value;
  JsonAllocator allocator;
  int status = jsonParse(source, &end_ptr, &value, &allocator);
  if (status != JSON_OK) {
    fprintf(stderr, "%s at %zd", jsonStrError(status), end_ptr - source);
    exit(EXIT_FAILURE);
  }
  sum_and_print(value);
  return 0;
}

double sum_and_print(JsonValue o) {
  double sum = 0;
  switch (o.getTag()) {
    case JSON_NUMBER:
      printf("%g\n", o.toNumber());
      sum += o.toNumber();
      break;
    case JSON_STRING:
      printf("\"%s\"\n", o.toString());
      break;
    case JSON_ARRAY:
      for (auto i : o) {
        sum += sum_and_print(i->value);
      }
      break;
    case JSON_OBJECT:
      for (auto i : o) {
        printf("%s = ", i->key);
        sum += sum_and_print(i->value);
      }
      break;
    case JSON_TRUE:
      fprintf(stdout, "TRUE");
      break;
    case JSON_FALSE:
      fprintf(stdout, "FALSE");
      break;
    case JSON_NULL:
      printf("NULL\n");
      break;
  }
  return sum;
}