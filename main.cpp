/* Trim fat from windows */
#define WIN32_LEAN_AND_MEAN
#pragma comment(linker, "/subsystem:windows")

/* Pre-processor directives */
#include <windows.h>

/* Windows Procedure Event Handler */
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
  PAINTSTRUCT paintStruct;
  /* Device Context */
  HDC hDC;
  /* Text for display */
  char string[] = "Hello, World!";
  /* Switch message, condition that is met will execute */
  switch(message)
  {
    /* Window is being created */
    case WM_CREATE:
      return 0;

    /* Window is closing */
    case WM_CLOSE:
      PostQuitMessage(0);
      return 0;

    /* Window needs update */
    case WM_PAINT:
      hDC = BeginPaint(hwnd, &paintStruct);

      /* Set txt color to blue */
      SetTextColor(hDC, COLORREF(0x00FF0000));

      /* Display text in middle of window */
      TextOut(hDC, 150, 150, string, sizeof(string) - 1);
      EndPaint(hwnd, &paintStruct);
      return 0;
  }
  return (DefWindowProc(hwnd,message,wParam,lParam));
}

/* Main function */
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
  WNDCLASSEX windowClass; //window class
  HWND hwnd; //window handle
  MSG msg; //message
  bool running; //flag saying when app is running

  /* Fill out the window class structure */
  windowClass.cbSize = sizeof(WNDCLASSEX);
  windowClass.style = CS_HREDRAW | CS_VREDRAW;
  windowClass.lpfnWndProc = WndProc;
  windowClass.cbClsExtra = 0;
  windowClass.cbWndExtra = 0;
  windowClass.hInstance = hInstance;
  windowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
  windowClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
  windowClass.lpszMenuName = NULL;
  windowClass.lpszClassName = "MyClass";
  windowClass.hIconSm = LoadIcon(NULL, IDI_WINLOGO);

  /* Register window class */
  if (!RegisterClassEx(&windowClass)) {
    return 0;
  }

  /* Class registered, so now create window */
  hwnd = CreateWindowEx(
      NULL, // extended style
      "MyClass", // class name
      "A Real Win App", // app name
      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU, // window style
      100, // x coords
      100, // y coords
      400, // width
      400, // height
      NULL, // handle to parent
      NULL, // handle to menu
      hInstance, // application instance
      NULL // no extra parameter's
  );

  /* Check if window creation failed */
  if (!hwnd) {
    return 0;
  }

  running = true; //initialize loop condition variable

  /* main message loop */
  while(running) {
    PeekMessage(&msg,NULL,NULL,NULL,PM_REMOVE);

    if (msg.message == WM_QUIT) { // check for a quit message
      running = false; // if found, quit app
    } else {
      /* Translate and dispatch to event queue */
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
  }
  return msg.wParam;
}