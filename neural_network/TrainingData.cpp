//
// Created by chromik on 01.09.2018.
//
#include "TrainingData.h"

TrainingData::TrainingData(const string filename) {
  m_trainingDataFile.open(filename.c_str());
}

TrainingData::~TrainingData() {
  m_trainingDataFile.close();
}

void TrainingData::getTopology(vector<unsigned> &topology) {
  string line, label;

  getline(m_trainingDataFile, line);
  stringstream ss(line);
  ss >> label;
  if (this->isEof() || label.compare("topology:") != 0) {
    abort();
  }

  while (!ss.eof()) {
    unsigned n;
    ss >> n;
    topology.push_back(n);
  }
}

bool TrainingData::isEof(void) {
  return m_trainingDataFile.eof();
}

unsigned TrainingData::getNextInputs(vector<double> &inputValues) {
  return getTargetValues(inputValues, "in:");
}

unsigned TrainingData::getTargetOutputs(vector<double> &targetOutputValues) {
  return getTargetValues(targetOutputValues, "out:");
}

unsigned TrainingData::getTargetValues(vector<double> &targetValues, string targetLabel) {
  targetValues.clear();

  string line;
  getline(m_trainingDataFile, line);
  stringstream ss(line);

  string label;
  ss >> label;

  if (label.compare(targetLabel) == 0) {
    double oneValue;
    while (ss >> oneValue) {
      targetValues.push_back(oneValue);
    }
  }

  return targetValues.size();
};