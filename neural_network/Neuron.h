//
// Created by chromik on 01.09.2018.
//
#ifndef UNTITLED_NEURON_H
#define UNTITLED_NEURON_H
#include <vector>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>
#include "Connection.h"
using namespace std;

class Neuron {
 public:
  Neuron(unsigned numOutputs, unsigned myIndex);
  void setOutputVal(double val);
  double getOutputVal(void) const;
  void feedForward(const vector<Neuron> &prevLayer);
  void calcOutputGradients(double targetValues);
  void calcHiddenGradients(const vector<Neuron> &nextLayer);
  void updateInputWeights(vector<Neuron> &prevLayer);

 private:
  static double eta; // [0.0...1.0] overall net training rate
  static double alpha; // [0.0...n] multiplier of last weight change [momentum]
  static double transferFunction(double x);
  static double transferFunctionDerivative(double x);
  // randomWeight: 0 - 1
  static double randomWeight(void) { return rand() / double(RAND_MAX); }
  double sumDOW(const vector<Neuron> &nextLayer) const;
  double m_outputVal;
  vector<Connection> m_outputWeights;
  unsigned m_myIndex;
  double m_gradient;
};

#endif