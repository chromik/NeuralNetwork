//
// Created by chromik on 01.09.2018.
//
#include "Net.h"

double Net::m_recentAverageSmoothingFactor = 100.0; // Number of training samples to average over

void Net::getResults(vector<double> &resultValues) const {
  resultValues.clear();
  for (unsigned n = 0; n < m_layers.back().size() - 1; ++n) {
    resultValues.push_back(m_layers.back()[n].getOutputVal());
  }
}

double Net::getRecentAverageError(void) const {
  return m_recentAverageError;
}

void Net::backProp(const std::vector<double> &targetValues) {
  // Calculate overal net error (RMS of output neuron errors)
  vector<Neuron> &outputLayer = m_layers.back();
  m_error = 0.0;

  for (unsigned n = 0; n < outputLayer.size() - 1; ++n) {
    double delta = targetValues[n] - outputLayer[n].getOutputVal();
    m_error += delta * delta;
  }
  m_error /= outputLayer.size() - 1; // get average error squared
  m_error = sqrt(m_error); // RMS

  // Implement a recent average measurement:
  m_recentAverageError = (m_recentAverageError * m_recentAverageSmoothingFactor + m_error) / (m_recentAverageSmoothingFactor + 1.0);

  // Calculate output layer gradients
  for (unsigned n = 0; n < outputLayer.size() - 1; ++n) {
    outputLayer[n].calcOutputGradients(targetValues[n]);
  }

  // Calculate gradients on hidden layers
  for (unsigned layerNum = m_layers.size() - 2; layerNum > 0; --layerNum) {
    vector<Neuron> &hiddenLayer = m_layers[layerNum];
    vector<Neuron> &nextLayer = m_layers[layerNum + 1];

    for (unsigned n = 0; n < hiddenLayer.size(); ++n) {
      hiddenLayer[n].calcHiddenGradients(nextLayer);
    }
  }

  // For all layers from outputs to first hidden layer,
  // update connection weights
  for (unsigned layerNum = m_layers.size() - 1; layerNum > 0; --layerNum) {
    vector<Neuron> &layer = m_layers[layerNum];
    vector<Neuron> &prevLayer = m_layers[layerNum - 1];

    for (unsigned n = 0; n < layer.size() - 1; ++n) {
      layer[n].updateInputWeights(prevLayer);
    }
  }
}

void Net::feedForward(const vector<double> &inputValues) {
  // Check the num of inputValues euqal to neuronnum expect bias
  assert(inputValues.size() == m_layers[0].size() - 1);

  // Assign {latch} the input values into the input neurons
  for (unsigned i = 0; i < inputValues.size(); ++i) {
    m_layers[0][i].setOutputVal(inputValues[i]);
  }

  // Forward propagate
  for (unsigned layerNum = 1; layerNum < m_layers.size(); ++layerNum) {
    vector<Neuron> &prevLayer = m_layers[layerNum - 1];
    for (unsigned n = 0; n < m_layers[layerNum].size() - 1; ++n) {
      m_layers[layerNum][n].feedForward(prevLayer);
    }
  }
}

Net::Net(const vector<unsigned> &topology) {
  unsigned numLayers = topology.size();
  for (unsigned layerNum = 0; layerNum < numLayers; ++layerNum) {
    m_layers.push_back(vector<Neuron>());
    // numOutputs of layer[i] is the numInputs of layer[i+1]
    // numOutputs of last layer is 0
    unsigned numOutputs = layerNum == topology.size() - 1 ? 0 : topology[layerNum + 1];

    // We have made a new Layer, now fill it ith neurons, and
    // add a bias neuron to the layer:
    for (unsigned neuronNum = 0; neuronNum <= topology[layerNum]; ++neuronNum) {
      m_layers.back().push_back(Neuron(numOutputs, neuronNum));
      cout << "Mad a Neuron!" << endl;
    }

    // Force the bias node's output value to 1.0. It's the last neuron created above
    m_layers.back().back().setOutputVal(1.0);
  }
}
