//
// Created by chromik on 01.09.2018.
//
#ifndef UNTITLED_TRAININGDATA_H
#define UNTITLED_TRAININGDATA_H
#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>
using namespace std;

class TrainingData {
 public:
  TrainingData(const string filename);
  ~TrainingData();
  bool isEof(void);
  void getTopology(vector<unsigned> &topology);
  // Returns the number of input values read from the file:
  unsigned getNextInputs(vector<double> &inputValues);
  unsigned getTargetOutputs(vector<double> &targetOutputValues);

 private:
  ifstream m_trainingDataFile;

 protected:
  unsigned getTargetValues(vector<double> &, string);
};

#endif //UNTITLED_TRAININGDATA_H
