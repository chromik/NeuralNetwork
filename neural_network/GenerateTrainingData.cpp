//
// Created by chromik on 02.09.2018.
//
#include <iostream>
#include <c++/4.8.3/fstream>
using namespace std;

int main()
{
  // random traning sets for XOR -- two inputs and one output

  ofstream out("C:\\Users\\chromik\\CLionProjects\\untitled\\neural_network\\trainingData.txt");

  out << "topology: 2 4 1" << endl;
  for (int i = 2000; i >= 0; --i) {
    int n1 = (int)(2.0 * rand() / double(RAND_MAX));
    int n2 = (int)(2.0 * rand() / double(RAND_MAX));
    int t = (n1 | n2); // should be 0 or 1
    out << "in: " << n1 << ".0 " << n2 << ".0 " << endl;
    out << "out: " << t << ".0" << endl;
  }
}

