//
// Created by chromik on 01.09.2018.
//
#ifndef UNTITLED_NET_H
#define UNTITLED_NET_H
#include <vector>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <fstream>
#include <sstream>
#include "Connection.h"
#include "Neuron.h"
using namespace std;

class Net {
 public:
  Net(const vector<unsigned> &topology);
  void feedForward(const vector<double> &inputValues);
  void backProp(const vector<double> &targetValues);
  void getResults(vector<double> &resultValues) const;
  double getRecentAverageError(void) const;

 private:
  vector<vector<Neuron>> m_layers; //m_layers[layerNum][neuronNum]
  double m_error;
  double m_recentAverageError;
  static double m_recentAverageSmoothingFactor;
};

#endif //UNTITLED_NET_H
