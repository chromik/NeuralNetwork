//
// Created by chromik on 01.09.2018.
//
#include "Neuron.h"
#include "TrainingData.h"
#include "Net.h"

double Neuron::eta = 0.15; // overall net learning rate
double Neuron::alpha = 0.5; // momentum, multiplier of last deltaWeight, [0.0..n]

void Neuron::updateInputWeights(vector<Neuron> &prevLayer) {
  /* The weights to be updated are in the Connection container
    in the neurons in the preceding layer */
  for (unsigned i = 0; i < prevLayer.size(); ++i) {
    Neuron &neuron = prevLayer[i];
    double oldDeltaWeight = neuron.m_outputWeights[m_myIndex].deltaWeight;

    /* Individual input magnified by the gradient and train rate:
      also add momentum = a fraction of the previous delta weight */
    double newDeltaWeight = eta * neuron.getOutputVal() * m_gradient + alpha * oldDeltaWeight;
    neuron.m_outputWeights[m_myIndex].deltaWeight = newDeltaWeight;
    neuron.m_outputWeights[m_myIndex].weight += newDeltaWeight;
  }
}
double Neuron::sumDOW(const vector<Neuron> &nextLayer) const {
  double sum = 0.0;

  // Sum our contributions of the errors at the nodes we feed
  for (unsigned n = 0; n < nextLayer.size() - 1; ++n) {
    sum += m_outputWeights[n].weight * nextLayer[n].m_gradient;
  }

  return sum;
}

void Neuron::calcHiddenGradients(const vector<Neuron> &nextLayer) {
  double dow = sumDOW(nextLayer);
  m_gradient = dow * Neuron::transferFunctionDerivative(m_outputVal);
}
void Neuron::calcOutputGradients(double targetValues) {
  double delta = targetValues - m_outputVal;
  m_gradient = delta * Neuron::transferFunctionDerivative(m_outputVal);
}

double Neuron::transferFunction(double x) {
  // tanh - output range [-1.0..1.0]
  return tanh(x);
}

double Neuron::transferFunctionDerivative(double x) {
  // tanh derivative
  return 1.0 - x * x;
}

void Neuron::feedForward(const vector<Neuron> &prevLayer) {
  double sum = 0.0;

  // Sum the previous layer's outputs (which are our inputs)
  // Include the bias node from the previous layer.

  for (unsigned n = 0; n < prevLayer.size(); ++n) {
    sum += prevLayer[n].getOutputVal() *
        prevLayer[n].m_outputWeights[m_myIndex].weight;
  }

  m_outputVal = Neuron::transferFunction(sum);
}

Neuron::Neuron(unsigned numOutputs, unsigned myIndex) : m_myIndex(myIndex) {
  for (unsigned c = 0; c < numOutputs; ++c) {
    m_outputWeights.push_back(Connection());
    m_outputWeights.back().weight = randomWeight();
  }
}

void Neuron::setOutputVal(double val) {
  m_outputVal = val;
}

double Neuron::getOutputVal(void) const {
  return m_outputVal;
}

void showVectorValues(string label, vector<double> &v) {
  cout << label << " ";
  for (unsigned i = 0; i < v.size(); ++i) {
    cout << v[i] << " ";
  }
  cout << endl;
}

int main() {
  TrainingData *trainData = new TrainingData("C:\\Users\\chromik\\CLionProjects\\untitled\\neural_network\\trainingData.txt");
  vector<unsigned> topology;

  trainData->getTopology(topology); // topology is defined on first line of data set
  Net myNet(topology);

  vector<double> inputValues, targetValues, resultValues;
  int trainingPass = 0;

  while (!trainData->isEof()) {
    ++trainingPass;
    cout << endl << "Pass " << trainingPass;

    // Get new input data and feed it forward:
    if (trainData->getNextInputs(inputValues) != topology[0]) {
      break;
    }
    showVectorValues(": Inputs :", inputValues);
    myNet.feedForward(inputValues);

    // Collect the net's actual results:
    myNet.getResults(resultValues);
    showVectorValues("Outputs:", resultValues);

    // Train the net what the outputs should have been:
    trainData->getTargetOutputs(targetValues);
    showVectorValues("Targets:", targetValues);
    assert(targetValues.size() == topology.back());

    myNet.backProp(targetValues);

    // Report how well the training is working, average over recnet
    cout << "Net recent average error: " << myNet.getRecentAverageError() << endl;
  }

  cout << endl << "Done" << endl;
  delete trainData;

}